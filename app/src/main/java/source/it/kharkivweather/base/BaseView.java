package source.it.kharkivweather.base;

public interface BaseView {
    void showProgress(boolean show);
    void showError(String error);
}
