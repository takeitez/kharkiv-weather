package source.it.kharkivweather.api;

import retrofit2.Call;
import source.it.kharkivweather.api.models.ForecastResponse;

public interface ApiManager {
    Call<ForecastResponse> loadForecast(int countDays);
}
