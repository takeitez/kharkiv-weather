package source.it.kharkivweather.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import source.it.kharkivweather.BuildConfig;
import source.it.kharkivweather.api.models.ForecastResponse;

public class ApiManagerImpl implements ApiManager {
    private final WeatherApi api;

    public ApiManagerImpl() {
        api = createRetrofit().create(WeatherApi.class);
    }

    private Retrofit createRetrofit() {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(BuildConfig.WEATHER_API_BASE_URL);
        builder.addConverterFactory(GsonConverterFactory.create());
        builder.client(createOkHttpClient());
        return builder.build();
    }

    private OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(createInterceptor());
        return builder.build();
    }

    private HttpLoggingInterceptor createInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Override
    public Call<ForecastResponse> loadForecast(int daysCount) {
        return api.loadForecast(
                BuildConfig.WEATHER_API_KEY,
                "Kharkiv",
                "json",
                daysCount
        );
    }
}
