package source.it.kharkivweather.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import source.it.kharkivweather.api.models.ForecastResponse;

public interface WeatherApi {
    @GET("weather.ashx")
    Call<ForecastResponse> loadForecast(
        @Query("key") String key,
        @Query("q") String q,
        @Query("format") String format,
        @Query("num_of_days") int daysCount
    );
}
