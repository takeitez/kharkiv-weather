package source.it.kharkivweather.forecast;

import source.it.kharkivweather.base.BasePresenter;

public interface ForecastPresenter extends BasePresenter<ForecastView> {
    void requestForecast(int daysCount);
}
