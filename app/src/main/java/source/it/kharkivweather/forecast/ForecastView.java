package source.it.kharkivweather.forecast;

import java.util.List;

import source.it.kharkivweather.base.BaseView;
import source.it.kharkivweather.models.Weather;

public interface ForecastView extends BaseView {
    void showForecast(List<Weather> forecast);
}
